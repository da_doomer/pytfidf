"""Source:
    https://en.wikipedia.org/w/index.php?title=Tf%E2%80%93idf&oldid=1011026818
"""
from typing import TypeVar
from collections import Counter

Term = TypeVar("Term")
Document = tuple[Term]
Corpus = set[Document]


def tf(term: Term, document: Document) -> float:
    counts = Counter(document)
    return counts[term]/sum(counts.values())


def idf(term: Term, corpus: Corpus) -> float:
    return len(corpus)/(1+len([d for d in corpus if term in d]))


def tfidf(term: Term, document: Document, corpus: Corpus) -> float:
    return tf(term, document)*idf(term, corpus)
