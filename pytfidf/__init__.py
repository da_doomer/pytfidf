__version__ = '0.1.0'
from .pytfidf import tf
from .pytfidf import idf
from .pytfidf import tfidf
