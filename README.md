# Pytfidf

One file pure-python implementation of the
[tf-idf](https://en.wikipedia.org/w/index.php?title=Tf%E2%80%93idf&oldid=1011026818)
statistic.

## Installation

Install using your usual package manager or simply download [pytfidf/pytfidf.py](pytfidf/pytfidf.py).

## Usage

```
from pytfidf import tfidf

document1 = {"one", "document"}
document2 = {"another", "document"},
corpus = {
	document1,
	document2
}

result1 = tfidf("one", document1, D)
result2 = tfidf("document", document2, D)

assert result1 > result2
```
